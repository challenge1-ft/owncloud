# Imagen de ubuntu 20.04
FROM ubuntu:20.04

#Directorio de trabajo 
WORKDIR /src

# Descarga de owncloud dentro del directorio
ADD https://github.com/owncloud/client/archive/refs/tags/v2.11.1.tar.gz .

# Se mueve lo descargado al bin y se le da los permisos necesarios
RUN mv ./v2.11.1.tar.gz /usr/local/bin/owncloud \
&& chmod +x /usr/local/bin/owncloud \
&& sudo apt-get update \
&& apt-get install -y \
&& apache2 \
&& libapache2-mod-php \
&& php-gd \
&& php-json \
&& php-mysql \
&& php-sqlite3 \
&& php-curl \
&& php-intl \
&& php-imagick \
&& php-zip \
&& php-xml \
&& php-mbstring \
&& php-soap \
&& php-ldap \
&& php-apcu \
&& php-redis \
&& php-dev \
&& libsmbclient-dev \
&& php-gmp \
&& smbclient

# ENTRYPOINT de ubuntu.
ENTRYPOINT [""]

